from types import NoneType
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.decorators import login_required

from recipes.forms import RatingForm


# from recipes.forms import RecipeForm
from recipes.models import Recipe, ShoppingItem, FoodItem


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 5


class UserListView(ListView):
    model = get_user_model()
    template_name = "recipes/userlist.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        foods = []
        for item in self.request.user.shopping_items.all():
            foods.append(item.food_item)
        context["foods"] = foods

        context["servings"] = self.request.GET.get("servings")

        # if self.request.GET.get("servings"):
        #     servings = self.request.GET.get("servings")
        # else:
        #     servings = self.get_object().servings

        # default_serving_size = self.get_object().servings
        # desired_serving_size = int(servings)
        # ratio = desired_serving_size / default_serving_size

        # context["servings"] = servings
        # context["ratio"] = ratio

        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "servings", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "servings", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "recipes/shoppinglist.html"
    context_object_name = "shopping_list"
    login_url = "/login/"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


@login_required(login_url="accounts/login/")
def shopping_item_create(request, pk, recipe_id):
    if request.method == "POST" and pk:
        item = ShoppingItem(
            user=request.user, food_item=FoodItem.objects.get(pk=pk)
        )
        item.save()
        return redirect("recipe_detail", pk=recipe_id)
    else:
        return redirect("recipes_list")


@login_required(login_url="accounts/login/")
def shopping_item_delete(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_item_list")
