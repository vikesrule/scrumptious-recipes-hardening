from ast import Not
from django import template

register = template.Library()


def resize_to(ingredient, target):
    num_servings = ingredient.recipe.servings
    if num_servings is not None and target is not None:
        try:
            ratio = int(target) / num_servings
            return ratio * ingredient.amount
        except ValueError:
            pass
    return ingredient.amount


register.filter("resize_to", resize_to)
