from django.urls import path

from recipes.views import (
    RecipeCreateView,
    RecipeDeleteView,
    RecipeUpdateView,
    log_rating,
    RecipeDetailView,
    RecipeListView,
    UserListView,
    ShoppingItemListView,
    shopping_item_create,
    shopping_item_delete,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("users/", UserListView.as_view(), name="user_list"),
    path(
        "shopping_items/",
        ShoppingItemListView.as_view(),
        name="shopping_item_list",
    ),
    path(
        "shopping_items/create/<int:pk>/<int:recipe_id>/",
        shopping_item_create,
        name="shopping_item_create",
    ),
    path(
        "shopping_items/delete/",
        shopping_item_delete,
        name="shopping_item_delete",
    ),
]
