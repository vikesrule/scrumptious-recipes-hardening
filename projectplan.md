## Requirements Doc

## Code Location
[x] 1. Code should be in the recipes app

## Required Things
1. ShoppingItem Model
    [x] 1. Should have user and fooditem
    [x] 2. combination of the two values should be unique, user can only add food item once to list
    [x] 3. one user can have many shopping items, one shopping item can have one user. need foreign key from shopping item model to user model
    [x] 4. one shopping item can have one food item, one food item can be on many different users shopping lists. need many-to-many relationship from food item to shopping item with foreign key. refer to ingredient model to see how to create foreign key relationship to fooditem
2. Shopping list paths
    [x] 1. 1st path - add shopping item to user's shopping list "recipes/shopping_items/create/"
    [x] 2. 2nd path - show list of shopping items on the user's shopping list "recipes/shopping_items/"
    [x] 3. 3rd path - remove all shopping items from a user's shopping list "recipes/shopping_items/delete/"
        4. Note - don't include "recipes" part in those paths for url.py file
3. ListView - recipes/shopping_items/
    [x] 1. Show a list of all shopping items that were created by the user, NOT show ALL shopping items.
    [x] 2. Button that deletes all items from the user's shopping list
4. CreateView - recipes/shopping_items/create
    [x] 1. handles ONLY HTTP POST requests (no html template for it), it should create a ShoppingItem instance in the database based on the current user and the value of the submitted "ingredients_id" value
    [x] 2. after creation, re-direct back to the recipe page it came from
5. DeleteView - recipes/shopping_items/delete
    [x] 1. handles ONLY HTTP POST requests (no html template), it should delete ShoppingItem instances associated with the current user.
    [x] 2. then, show the empty shopping list after deletion

