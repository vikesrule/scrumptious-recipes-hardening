## Requirements Doc

## Code Location
[ ] 1. Code should be in the recipes app

## Required Things
1. Recipe Model
    [ ] 1. Add new property "servings" to model
        [ ] 1. Should be a whole number value > 0
2. UpdateServings View
    [ ] 1. Add "servings #" above the Rating form
    [ ] 2. Changing servings amount should adjust the "Serves #"
    [ ] 3. Should adjust the food item amount based on serving quantity (ie: 2 apples 1 serving, 4 apples 2 servings, etc.)
    

